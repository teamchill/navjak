#ifndef _SENSORGROUP_H_
#define _SENSORGROUP_H_

#include "Sensor.h"

class SensorGroup {
 public:
  Sensor *sensor; // Sensor to work with.
  int min_value;
  int max_value; // Known min/max values.
  int threshold; // Threshold value. Must be between min and max.

 SensorGroup(Sensor *s, int min, int max, int thresh) {
    sensor = s;
    this->min_value = min;
    this->max_value = max;
    threshold = thresh;
  }
};

#endif