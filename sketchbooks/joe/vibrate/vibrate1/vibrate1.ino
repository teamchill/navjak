/*
  Simple vibrating motor sketch.
  Connect red wire to BUZZ_PIN output and blue to ground.  
*/
int BUZZ_PIN = 6;

void setup(){
  
}

void loop(){
  analogWrite(BUZZ_PIN, 250); 
  delay(1000);
  analogWrite(BUZZ_PIN, 0);
  delay(1000);
}
