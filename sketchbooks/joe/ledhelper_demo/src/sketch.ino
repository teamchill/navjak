#include <LED_Ring.h>

#include <PerPixelRingSetting.h>
#include <FullColorRingSetting.h>

#define PIN 6
//#define DEBUG

LED_Ring ring = LED_Ring(PIN);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

FullColorRingSetting orange;
FullColorRingSetting off;

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println(F("Initializing."));
#endif

  ring.init();

  Serial.println(F("Brightness"));
  ring.setBrightness(10);

  Serial.println(F("Orange"));
  orange.setWipeColor(0xFF, 0xFF, 0X00);
  orange.delay = 1000;
  orange.brightness = 10;

  Serial.println(F("Off"));
  off.setWipeColor(0);
  off.brightness = 0;
  off.delay = 2500;

  ring.addConf(&orange);
  ring.addConf(&off);

  ring.run(true);

#ifdef DEBUG
  Serial.println(F("Mem use (approx, in bytes): "));
  Serial.println(sizeof(ring));
  Serial.println(sizeof(off));
  Serial.println(sizeof(orange));
  Serial.print(F("Total: "));
  Serial.println(sizeof(ring)+sizeof(off)+sizeof(orange));
  Serial.println(F("Starting loop..."));
#endif
  delay(1000);
}

void loop() {
#ifdef DEBUG
  Serial.print(F("** LOOP "));
  Serial.print(millis());
  Serial.println(F(" **"));
#endif
  // Run update loop.
  ring.update();
  delay(1000);
#ifdef DEBUG
  Serial.print(F("*** END LOOP ("));
  Serial.print(millis());
  Serial.println(F(" ***"));
#endif
}
