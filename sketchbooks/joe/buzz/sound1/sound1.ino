int SOUND_PIN = 6;
int LED_PIN = 13;

void setup(){
   pinMode(LED_PIN, OUTPUT);
}

void loop(){
  analogWrite(SOUND_PIN, 250);
  digitalWrite(LED_PIN, HIGH);
  delay(2000);
  analogWrite(SOUND_PIN, 0);
  digitalWrite(LED_PIN, LOW);
  delay(2000);
}
