#include <Adafruit_NeoPixel.h>

//Flex Sensor Pin (flexPin)
//the analog pin the Flex Sensor is connected to
int flexPin = 0;  

#define LED_PIN 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, LED_PIN, NEO_GRB + NEO_KHZ800);
    
void setup() {
  /*
  for (int i=4; i<14; i++){
    pinMode(i, OUTPUT); //sets the led pins 4 to 13 to output
  }*/
 Serial.begin(9600);
 
 strip.begin();
 strip.show();
}

void loop(){
 //Ensure to turn off ALL LEDs before continuing 
 /*
 for (int i=4; i<14; i++){
    digitalWrite(i, LOW); 
  }
 */
 /* Read the flex Level 
  Adjust the value 130 to 275 to span 4 to 13
  The values 130 and 275 may need to be widened to suit 
  the minimum and maximum flex levels being read by the 
  Analog pin */
 //int flexReading = map(analogRead(flexPin), 130, 275, 4, 13); 
// int flexReading = analogRead(flexPin);
  int flexReading = map(analogRead(flexPin), 300, 600, 0, 255);
  
  strip.setPixelColor(0, strip.Color(flexReading/255, flexReading, 0));
  strip.show();
         
// Make sure the value does not go beyond 4 or 13
 //int LEDnum = constrain(flexReading, 4, 13); 
 
/*Call the blink function: this will turn the LED on for 10 milliseconds, and keep it
  off for only 1 millisecond. You can change the blink rate by changing these values,
  however, I want a quick response time when the flex sensor bends, hence the small 
  values. LEDnum determines which LED gets turned on.*/
 //blink(LEDnum, 10,1);
 
  //Serial.println(flexReading);
  //delay(1000);
}

// The blink function - used to turn the LEDs on and off
void blink(int LEDPin, int onTime, int offTime){
  // Turn the LED on                                         
 digitalWrite(LEDPin, HIGH);  
 
  // Delay so that you can see the LED go On.
 delay(onTime);
 
  // Turn the LED Off                                         
  digitalWrite(LEDPin, LOW);  
 
 // Increase this Delay if you want to see an actual blinking effect.
  delay(offTime);
}
