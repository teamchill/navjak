#include "Sensor.h"

Sensor::Sensor(uint8_t input_pin, uint32_t delay, int8_t power_pin) : _pin(input_pin), _delay(delay) {
  if (power_pin >= 0){
    pinMode(power_pin, OUTPUT);
    digitalWrite(power_pin, HIGH);
  }
}

void Sensor::update(){
  analogRead(_pin); // Discard the first reading.
  delay(_delay); // Wait
  _reading = analogRead(_pin); // Get a (better?) reading.
}

int Sensor::getReading() const {
  return _reading;
}

uint8_t Sensor::getPin() const {
  return _pin;
}
