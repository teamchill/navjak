/**
 * Generic sensor class. Hooks up to an input pin and performs readings.
 * Supports delayed readings for more precise results.
 **/

#ifndef _SENSOR_H_
#define _SENSOR_H_

#include <stdint.h>
#include "Arduino.h"

class Sensor {
 public:
  /**
   * Initializes the sensor on a specific input pin along with suggested delay
   * between a disposable reading to a proper reading.
   * Optionally add a specific pin to initialise power for the Sensor. If you
   * set up power in your own code (pinMode, digitalWrite, etc) then ignore.
   **/
  Sensor(uint8_t input_pin, uint32_t delay = 10, int8_t power_pin = -1);

  /**
   * Performs readings and stores them for later retrieval. This call takes
   * slightly longer than the delay given during initialization.
   **/
  void update();
  
  /**
   * Returns the latest reading.
   **/
  int getReading() const;

  uint8_t getPin() const;

 private:
  uint8_t _pin;
  int _reading;
  uint32_t _delay;
};

#endif
