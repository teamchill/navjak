/* FlexTriggers are min/max bounds for a flex sensor coupled with a pretty
 * open-ended callback to call if the trigger holds true. Simple struct.
 **/

#ifndef _FLEXTRIGGER_H_
#define _FLEXTRIGGER_H_

#define INT_MAX 32767
#define INT_MIN -32768

typedef void(*flextrigger_callback)(void);

struct FlexTrigger {
  FlexTrigger(flextrigger_callback cb, int min = INT_MIN, int max = INT_MAX);

  int min;
  int max;
  flextrigger_callback callback;
};

#endif
