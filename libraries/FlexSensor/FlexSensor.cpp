#include "FlexSensor.h"

FlexSensor::FlexSensor(uint32_t pin, uint32_t history_size) : _firstTrigger{nullptr}, _lastTrigger{nullptr}, _readingSize{history_size} {
  pinMode(pin, INPUT);

  _readings = new int32_t[history_size];
  for (auto i = 0; i < history_size; i++) _readings[i] = 0; // Reset readings.
}

FlexSensor::~FlexSensor() {
  delete _firstTrigger;
}

void FlexSensor::addTrigger(FlexTrigger* ft) {
  // Initialize.
  FlexTriggerLink* ftl = new FlexTriggerLink(ft); 

  // Special case.
  if (_firstTrigger == nullptr) {
    _firstTrigger = ftl;
    _lastTrigger = ftl->next;
  } else {
    _lastTrigger->next = ftl;
    _lastTrigger = ftl;
  }
}

bool FlexSensor::withinBounds(const FlexTrigger& ft){
  const int& cr = this->reading();
  return (ft.min <= cr && cr <= ft.max);
}

int32_t FlexSensor::reading(uint32_t index){
  if (index >= _readingSize) return _readings[_readingSize-1];
  else return _readings[index];
};

void FlexSensor::process() {
  if (_firstTrigger == nullptr) return; // No triggers.

  FlexTriggerLink* tmp;
  tmp = _firstTrigger;

  do {
    if (withinBounds(*tmp->value)) tmp->value->callback();
    tmp = tmp->next;
  } while (tmp->next != nullptr);
}

void FlexSensor::updateValues() {
  for (auto i = _readingSize; i > 0; i++)
    _readings[i] = _readings[i-1];

  // For best read results, do a read, wait 10ms, and store a read.
  analogRead(_pin);
  delay(10);
  _readings[0] = analogRead(_pin);
}
