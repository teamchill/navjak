/*
  Library for handling the flex sensors. the FlexSensor class tracks a flex
  sensor on a specific pin and supports callbacks when certain trigger
  conditions are met.
 */

#ifndef _FLEXHELPER_H_
#define _FLEXHELPER_H_

#include <stdint.h>
#include <Arduino.h>
#include "LinkedList.h"
#include "FlexTrigger.h"

typedef navjak::Link<FlexTrigger*> FlexTriggerLink;

class FlexSensor {
 public:
  /**
   * Initialize a FlexSensor bound to a specific input pin. This assumes that
   * the sensor is powered through a regular voltage output.
   **/
  FlexSensor(uint32_t pin, uint32_t history_size = 1);

  ~FlexSensor();

  /**
   * Runs the FlexSensor's minor cycle. It reads the current input, checks to
   * see if the value fits within any triggers, and calls them as needed.
   **/
  void process();

  /**
   * Adds a new trigger to the list of Flex triggers.
   **/
  void addTrigger(FlexTrigger* ft);

  /**
   * Checks if the current reading lies within the bounds of a given
   * FlexTrigger.
   **/
  bool withinBounds(const FlexTrigger& ft);

  /**
   * Retrieves a given reading. Lower index is a more current reading. Index
   * cannot be higher than the size of the history.
   **/
  int32_t reading(uint32_t index = 0);

 private:
  /**
   * Updates the current readings. It shifts all previous readings once
   * (discarding the oldest reading) and reads a new value from the flex sensor.
   * This operation takes at least 10ms due to how a stable reading is best
   * achieved.
   **/
  void updateValues();

  /**
   * Link to the first of our triggers. We just go front-to-back when checking
   * them.
   **/
  FlexTriggerLink* _firstTrigger;

  /**
   * Link to the last of our triggers. This makes adding to the list a hell of a
   * lot cheaper.
   **/
  FlexTriggerLink* _lastTrigger;

  uint32_t _pin;

  /**
   *
   **/
  int32_t* _readings;

  uint32_t _readingSize;
};

#endif
