#ifndef _FULLCOLORRINGSETTING_H
#define _FULLCOLORRINGSETTING_H

#include "RingSetting.h"

/**
 * RingSetting that allows full color settings. It mimics a 16-light array but only uses a single pixel value, reducing memory used by 56 bytes.
 **/
class FullColorRingSetting : public RingSetting {
 public:
  // Pure virtual. Overloaded in PerPixelRingSetting and FullColorRingSetting.
  virtual uint32_t getColor(uint32_t i);
  
  // Sets all pixels to the same colour. In this class, proxies setWipeColor.
  virtual void setPixelColor(uint32_t i, uint8_t r, uint8_t g, uint8_t b);
  virtual void setPixelColor(uint32_t i, uint32_t c);

  // Sets all pixels to the same colour.
  virtual void setWipeColor(uint8_t r, uint8_t g, uint8_t b);
  virtual void setWipeColor(uint32_t c);

 private:
  uint32_t _color;
};

#endif
