#include "FullColorRingSetting.h"

uint32_t FullColorRingSetting::getColor(uint32_t i){
  return this->_color;
}

void FullColorRingSetting::setPixelColor(uint32_t i, uint8_t r, uint8_t g, uint8_t b){
  this->setWipeColor(r, g, b);
}

void FullColorRingSetting::setPixelColor(uint32_t i, uint32_t c){
  this->setWipeColor(c);
}

void FullColorRingSetting::setWipeColor(uint8_t r, uint8_t g, uint8_t b){
  this->_color = this->color(r, g, b);
}

void FullColorRingSetting::setWipeColor(uint32_t c){
  this->_color = c;
}
