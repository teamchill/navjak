#include "RingSetting.h"

RingSetting::RingSetting() {
  this->brightness = 0;
  this->delay = 1000;  
  this->ledCount = 16;
  this->nextDelay = 0;
}

RingSetting::RingSetting(int led_count)  {
  this->brightness = 0;
  this->delay = 1000;  
  this->ledCount = led_count;
  this->nextDelay = 0;
}

// SHAMELESSLY ripped from the Adafruit_NeoPixel code base.
// TODO: Check Adafruit_NeoPixel's license and ensure we're in the clear or what
// may be needed to conform.
uint32_t RingSetting::color(uint8_t r, uint8_t g, uint8_t b){
  return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}
