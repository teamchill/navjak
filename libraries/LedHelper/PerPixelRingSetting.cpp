#include "PerPixelRingSetting.h"

PerPixelRingSetting::PerPixelRingSetting() {
  this->_colors = new uint32_t[16];
}

PerPixelRingSetting::PerPixelRingSetting(int led_count){
  this->_colors = new uint32_t[led_count];
}

PerPixelRingSetting::~PerPixelRingSetting(){
  delete [] this->_colors;
}

uint32_t PerPixelRingSetting::getColor(uint32_t i){
  return this->_colors[i];
}

void PerPixelRingSetting::setColor(uint32_t i, uint8_t r, uint8_t g, uint8_t b){
  // If index is out of range, fail.
  if (i >= this->ledCount) return; // We have no exceptions. C-style return int?
  this->_colors[i] = this->color(r, g, b);
}

void PerPixelRingSetting::setColor(uint32_t i, uint32_t c){
  this->_colors[i] = c;
}

void PerPixelRingSetting::setWipeColor(uint8_t r, uint8_t g, uint8_t b){
  this->setWipeColor(this->color(r, g, b));
}

void PerPixelRingSetting::setWipeColor(uint32_t c){
  for (int i = 0; i < this->ledCount; i++){
    this->_colors[i] = c;
  }
}
