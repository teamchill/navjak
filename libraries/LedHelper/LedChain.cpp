#include "LedChain.h"

//#define DEBUG

LedChain::LedChain(int led_count, int pin) : Adafruit_NeoPixel(led_count, pin) {
  this->begin();
  _active = false;
}

void LedChain::run(){
  _active = true;
  _timeToSwitch = _ledSetting->delay;
  _waitingToTurnOn = false;
}

void LedChain::update(){
  // Yield immediately if we are inactive.
  if (!this->_active){
    #ifdef DEBUG
    Serial.println(F("LedChain: inactive!"));
    #endif
    return;
  } /* test for update */ else if (millis() > _timeToSwitch){
    // If we're already off waiting for the next conf, or if the nextDelay is 0,
    // switch immediately.
    if (_waitingToTurnOn || _ledSetting->nextDelay == 0){
      #ifdef DEBUG
      Serial.println(F("TURN ON!"));
      #endif
      activateConf();
      _timeToSwitch = millis() + _ledSetting->delay;
      _waitingToTurnOn = false;
    } else {
      // Turn off and wait for next.
      #ifdef DEBUG
      Serial.println(F("TURN *OFF*!"));
      #endif
      turnOff();
      _timeToSwitch = millis() + _ledSetting->nextDelay;
      _waitingToTurnOn = true;
    }
  }
}

void LedChain::turnOff(){
  for (int i = 0; i < numPixels(); i++){
    setPixelColor(i, this->Color(0, 0, 0));
  }
  show();
}

void LedChain::setConf(RingSetting *setting, bool activate){
  _ledSetting = setting;

  if (activate) activateConf();
}

void LedChain::activateConf(){
  #ifdef DEBUG
  Serial.print(F("Setting conf... LED("));
  #endif DEBUG
  for (int i = 0; i < _ledSetting->ledCount; i++){
    this->setPixelColor(i, _ledSetting->getColor(i)); 
  }
  this->setBrightness(_ledSetting->brightness);
  #ifdef DEBUG
  Serial.println(F(") ... show!"));
  #endif
  show();
}

void LedChain::stop(){
  this->_active = false;
}
