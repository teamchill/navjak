#ifndef _RING_SETTING_H
#define _RING_SETTING_H

#include <Arduino.h>

class RingSetting {
 public:
  // Creates a new ring setting with 16 LED lights.
  RingSetting();
  // Creates a new ring setting with the specified number of lights.
  RingSetting(int led_count);

  // We keep these variables public because RingSetting is used as a container
  // rather than hooking it up to the NeoPixel lib itself.
  // The delay (in millis) to persist the setting.
  int delay;

  /**
     If non-zero, the lights are turned off for this amount of time before the
     next setting is activated.
  **/
  int nextDelay;

  // The brightness config.
  int brightness;

  // Number of LEDs represented by this setting.
  int ledCount;

  // Pure virtual. Overloaded in PerPixelRingSetting and FullColorRingSetting.
  virtual uint32_t getColor(uint32_t i) = 0;
  // Retrieve an entire configuration as a static array reference.
  // Cute idea but it doesn't jive with FullColorRingSetting's data model.
  // const uint32_t (&getColorConf())[16];
  
  // Sets all pixels to the same colour.
  virtual void setWipeColor(uint8_t r, uint8_t g, uint8_t b) = 0;
  virtual void setWipeColor(uint32_t c) = 0;

  // Sets a single pixel to a specific colour.
  virtual void setPixelColor(uint32_t index, uint8_t r, uint8_t g, uint8_t b) = 0;
  virtual void setPixelColor(uint32_t index, uint32_t c) = 0;

  // Returns a combined 32-bit int representing the three passed color values.
  static uint32_t color(uint8_t r, uint8_t g, uint8_t b);
};

#endif
