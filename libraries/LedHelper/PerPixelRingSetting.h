#ifndef _PERPIXELRINGSETTING_H
#define _PERPIXELRINGSETTING_H

#include "RingSetting.h"

/**
 * RingSetting that allows you to set colors per pixel. Using a setting is relatively expensive (the pixel data alone takes up 64 bytes of SRAM memory) but does confer
a high level of flexiblity (for example, the lights can be changed without addditional cost).
 **/
class PerPixelRingSetting : public RingSetting {
 public:
  PerPixelRingSetting();
  PerPixelRingSetting(int led_count);

  ~PerPixelRingSetting();

  // Pure virtual. Overloaded in PerPixelRingSetting and FullColorRingSetting.
  virtual uint32_t getColor(uint32_t i);
  
  // Sets a single pixel to a specific colour.
  virtual void setColor(uint32_t index, uint8_t r, uint8_t g, uint8_t b);
  virtual void setColor(uint32_t index, uint32_t c);

  // Sets all pixels to the same colour.
  virtual void setWipeColor(uint8_t r, uint8_t g, uint8_t b);
  virtual void setWipeColor(uint32_t c);

 private:
  uint32_t* _colors;
};

#endif // _PERPIXELRINGSETTING_H
