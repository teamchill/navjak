#ifndef _LED_RING_H
#define _LED_RING_H

#include <Adafruit_NeoPixel.h>
#include "RingSetting.h"

class LedChain : public Adafruit_NeoPixel {
 public:
  LedChain(int led_count, int pin);

  // Set the configuration used by the chain.
  void setConf(RingSetting* setting, bool activate = true);
  // Force on the set configuration.
  void activateConf();

  // Instructs the LedChain to prepare for running configurations. This will
  // enable calls to update.
  void run();
  // Called whenever possible to allow the LedChain to update. It will yield
  // quickly if the current RingSetting's delay is not yet up.
  void update();
  // Instructs the LedChain to halt any actions. This disables calls to update.
  void stop();

  // Sets the color of a NeoPixel, overriding any active RingSetting for that
  // pixel. This *will* be overwritten with the following
  void setColor(int r, int g, int b, bool instant = false);
  void setColor(uint32_t c, bool instant = false);

  // Turn of all the LED lights.
  void turnOff();

 private:
  // Is the chain currently active, i.e. running its sequences?
  bool _active;

  // Assuming active, are we displaying a setting or delaying until the next?
  bool _waitingToTurnOn;

  // The count of millis() at which point it is time to switch state.
  int _timeToSwitch;

  // Number of LED lights in our chain.
  int _ledCount;

  RingSetting *_ledSetting;
};

#endif
