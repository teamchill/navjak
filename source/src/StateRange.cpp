#include "StateRange.h"

StateRange::StateRange(int e_min, int e_max, int s_min, int s_max) : elbow_range{e_min, e_max}, shoulder_range{s_min, s_max} {}

bool StateRange::withinRange(int e, int s) const {
  const Range<int>& er = elbow_range; // Shorthand
  const Range<int>& sr = shoulder_range; // Also shorthand
  return
    er.min <= e && e <= er.max &&
    sr.min <= s && s <= sr.max;
}
