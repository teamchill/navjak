#ifndef _STATERANGE_H_
#define _STATERANGE_H_

#include "Range.h"

// Couples ranges with a state.
struct StateRange {
  StateRange(int e_min, int e_max, int s_min, int s_max);

  Range<int> elbow_range;
  Range<int> shoulder_range;

  /**
   * Returns true if e is within the elbow ranges and s is within the shoulder
   * ranges.
   **/
  bool withinRange(int e, int s) const;
};

#endif
