#include "LedHelper.h"
#include "Sensor.h"
#include "LinkedList.h"
#include "StateRange.h"
#include "LEDStates.h"
#include "Adafruit_NeoPixel.h"
#include "SensorGroup.h"

/** SENSORS **/

Sensor rightElbowSensor(A10),
  rightShoulderSensor(A9);

SensorGroup rightElbow(&rightElbowSensor, 60, 85, 70),
  rightShoulder(&rightShoulderSensor, 60, 85, 70);

FullColorRingSetting OrangeBlink, RedStop, OffSetting, DriveLight;

RingSetting *currentLeftSetting = NULL,
  *currentRightSetting = NULL,
  *leftSetting = NULL,
  *rightSetting = NULL;

LedChain TestLed(16, 6),
  &leftLed = TestLed,
  &rightLed = TestLed;

#define NO_SETTING 0
#define ORANGE_SETTING 1
#define RED_SETTING 2

RingSetting* getSettingForReading(SensorGroup &elbow, SensorGroup &shoulder){
  int er = elbow.sensor->getReading();
  int sr = shoulder.sensor->getReading();

  Serial.print(F("Sensor("));
  Serial.print(er);
  Serial.print(F(","));
  Serial.print(sr);
  Serial.print(F("):"));

  if (sr >= shoulder.threshold){
    if (er >= elbow.threshold){
      Serial.println(F("BLINK"));
      return &OrangeBlink;
    }
    if (er <  elbow.threshold){
      Serial.println(F("BRAKE"));
      return &RedStop;
    }
  }

  Serial.println("OFF");
  return &DriveLight;
}

void setup()
{
  Serial.begin(9600);

  Serial.println("Setup!");

  //LeftLedRing.turnOff();
  //RightLedRing.turnOff();

  TestLed.show();
  
  OrangeBlink.setWipeColor(0xFF8800);
  OrangeBlink.brightness = 50;
  OrangeBlink.delay = 1000;
  OrangeBlink.nextDelay = 1000;
 
  RedStop.setWipeColor(0xFF0000);
  RedStop.brightness = 50;
  RedStop.delay = 5000;
  RedStop.nextDelay = 0;

  OffSetting.setWipeColor(0x000000);
  OffSetting.brightness = 10;
  OffSetting.delay = 5000;
  OffSetting.nextDelay = 0;
 
  DriveLight.setWipeColor(0x0000FF);
  DriveLight.brightness = 50;
  DriveLight.delay = 5000;
  DriveLight.nextDelay = 0;

  Serial.println("End setup. 1 second delay to loop.");
  delay(1000);
}

void loop()
{
  rightSetting = getSettingForReading(rightElbow, rightShoulder);
  if (rightSetting != currentRightSetting){
    currentRightSetting = rightSetting;
    Serial.print(F("New setting for right("));
    rightLed.setConf(rightSetting);
    leftLed.setConf(rightSetting);
  }

  rightLed.update();
  leftLed.update();
  
  delay(50);
}
