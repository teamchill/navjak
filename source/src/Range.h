#ifndef _RANGE_H_
#define _RANGE_H_

template <typename N>
struct Range {
  N min, max;
};

#endif
