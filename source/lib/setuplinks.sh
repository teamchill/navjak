#!/bin/sh

echo "Setting up symbolic links to libs..."

ln -s ../../libraries/LedHelper/ ./LedHelper
ln -s ../../libraries/LinkedList/ ./LinkedList
ln -s ../../libraries/Sensor/ ./Sensor

echo "DONE."
